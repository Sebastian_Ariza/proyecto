$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $('#modal').on('show.bs.modal', function (e) {
        $('#btnModal').removeClass('btn-primary');
        $('#btnModal').addClass('btn-secondary');
        console.log('El modal se está abriendo');
      });

    $('#modal').on('shown.bs.modal', function (e) {
        console.log('El modal se ha abierto');
      });

    $('#modal').on('hide.bs.modal', function (e) {
        $('#btnModal').removeClass('btn-secondary');
        $('#btnModal').addClass('btn-primary');
        console.log('El modal se está cerrando');
      });

    $('#modal').on('hidden.bs.modal', function (e) {
        console.log('El modal se ha cerrado');
      });

    $('#cotizar').click(function () {
        console.log('Se ha enviado la cotización');
      });
  });

  $('.carousel').carousel({
      interval: 3000
  })